export { }
function createrow() {
    var row = (document.getElementById("rows") as HTMLInputElement).value;
    
    var t = "t";
    if (row != 0) // check whether the selected rows input is valid or not
    {
        
        // creating the dynamic table 
        var tbl = document.createElement("table");
        var tblBody = document.createElement("tbody");
        //header of table 
        var row1 = document.createElement("tr");
        //header of 1st column
        var cell1 = document.createElement("th");
        var cellText1 = document.createTextNode("Value");
        cell1.appendChild(cellText1);
        row1.appendChild(cell1);
        //header of 2nd column
        var cell2 = document.createElement("th");
        var cellText2 = document.createTextNode("Cube");
        cell2.appendChild(cellText2);
        row1.appendChild(cell2);
        row1.setAttribute("class", "heading");
        tblBody.appendChild(row1);
        //creating dynamic rows of column
        for (var i = 0; i < row; i++) {
            var row_1 = document.createElement("tr");
            for (var j = 0; j < 2; j++) {
                var cell = document.createElement("td");
                cell.setAttribute("class", "talign");
                if (j == 0) {
                    var t_1 = document.createElement("input");
                    t_1.setAttribute("type", "text");
                    t_1.setAttribute("id", i);
                    t_1.setAttribute("class", "tmargin");
                    cell.appendChild(t_1);
                }
                else {
                    cell.setAttribute("id", t + i);
                }
                row_1.appendChild(cell);
            }
            tblBody.appendChild(row_1);
        }
        tbl.appendChild(tblBody);
        (document.getElementById("table-area") as HTMLInputElement).innerHTML = tbl.innerHTML;
        //html input type button 
        var bu = "<input type='button' value='Find Cube' class='btn' onclick='cube()'>";
        (document.getElementById("submit") as HTMLInputElement).innerHTML = bu;
    }
    else // if selected rows less than 2 then throw error message
    {
        alert("Please select number of rows");
        (document.getElementById("table-area") as HTMLInputElement).innerHTML = "";
        (document.getElementById("submit") as HTMLInputElement).innerHTML = "";
    }
}
//function to find cube
function cube() {
    var row = (document.getElementById("rows") as HTMLInputElement).value;
   
    var t = "t";
    var flag = 0;
    let i: number;
    for (i = 0; i < row; i++) {
        var ele = (document.getElementById(i) as HTMLInputElement).value;
        typeof ele
        if (ele.length == 0) {
            flag = 1;
        }
        else {
          
            ele = ele * ele * ele;

            (document.getElementById(t + i) as HTMLInputElement).innerHTML = ele;
        }
    }
    if (flag == 1) // if cells/rows are empty then throw error
    {
        alert("Row is empty...!");
    }
}
